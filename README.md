# README

## Setting up the application

```sh
git clone simple_bank.bundle
bin/setup
rails bank:setup
open http://localhost:3000
rails s
```

Visit app in heroku https://villu164-simple-bank.herokuapp.com/

## Deployment to heroku
Roughly based on Heroku's article [Getting Started on Heroku with Rails 6.x](https://devcenter.heroku.com/articles/getting-started-with-rails6)
```sh
# Init heroku
heroku create
# Push code to heroku repo
git push heroku master
# Run migrations
heroku run rails db:migrate
# Create bank account
heroku run rails bank:setup

#open page
heroku open

# optionally when in testing phase
heroku run rails account:dummies

# optionally rename your app to a more suitable one
# heads up
#  ▸    Name must start with a letter, end with a letter or digit and can only contain lowercase letters, digits, and dashes.

heroku apps:rename villu164-simple-bank

```
