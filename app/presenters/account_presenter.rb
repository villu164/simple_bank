# frozen_string_literal: true

class AccountPresenter < ApplicationPresenter
  def name
    "#{currency} #{number} - #{internal? ? 'BANK' : (user_email || 'Mr. Nobody')}"
  end
end
