# frozen_string_literal: true

class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.blank?
    return if value =~ /\A([\w+\-]\.?)+(?<!\.)@[a-z\d][a-z\d\-]*(\.[a-z\d\-]+)*\.[a-z]+\z/i

    record.errors.add(attribute, options[:message] || :invalid)
  end
end
