import { Controller } from 'stimulus'; 
export default class extends Controller {
  static targets = ["toggle"]

  toggle() {
    if (!this.hasToggleTarget) return;

    this.toggleTarget.classList.toggle(this.className);
  }

  get className() {
    return this.data.get('class-name') || 'hidden';
  }
}
