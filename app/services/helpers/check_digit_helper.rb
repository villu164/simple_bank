module Helpers
  class CheckDigitHelper
    WEIGHTS = ([7, 3, 1] * 4)[0, 10].reverse.freeze

    class << self
      def checksum731(code)
        digits = code.rjust(WEIGHTS.length, '0').split('').map(&:to_i)
        checksum = WEIGHTS.zip(digits).map { |a| a.reduce(:*) }.sum % 10
        checksum == 0 ? checksum : 10 - checksum
      end
    end
  end
end
