class FeeCalculatorService
  MIN_FEE = 5.to_d
  FEE_FROM_AMOUNT = 0.025
  include ActiveModel::Model
  attr_accessor :amount

  def amount
    @amount || 0
  end

  def fee
    [amount*FEE_FROM_AMOUNT, MIN_FEE].max.round(2)
  end

  def max_for_balance
    [[(amount / (1+FEE_FROM_AMOUNT)).floor(2), amount - MIN_FEE].min, 0].max
  end
end
