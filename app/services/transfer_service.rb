class TransferService
  include ActiveModel::Model
  attr_accessor :amount, :number
  attr_writer :transaction

  delegate :valid?, :errors, to: :transaction

  def transfer_from(account)
    self.transaction = account.transaction_orders.new(amount: amount, number: number)
    return unless valid?

    transaction.save
  end

  def transaction
    @transaction || TransactionOrder.new
  end
end
