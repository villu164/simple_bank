class TransactionSearchService
  include ActiveModel::Model
  attr_writer :start_date, :end_date
  validates :start_date, timeliness: { type: :date }, allow_blank: true
  validates :end_date, timeliness: { type: :date }, allow_blank: true

  def start_date
    safe_format_date(@start_date)
  end

  def end_date
    safe_format_date(@end_date)
  end

  private

  def safe_format_date(date)
    Date.parse(date)
  rescue
    Date.today
  end
end
