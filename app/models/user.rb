class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  after_create :create_account!, :deposit_10k

  has_one :account

  validates :email, presence: true, email: true, uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6 }

  def deposit_10k
    account.deposit(10_000)
  end
end
