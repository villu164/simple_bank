class TransactionOrder < ApplicationRecord
  include AASM

  belongs_to :account
  has_many :transactions

  validates :amount, presence: true, numericality: { greater_than_or_equal_to: 0.01, less_than_or_equal_to: :max_available_amount }
  validates :number, presence: true
  validates :state, presence: true
  validates :to_account, presence: true

  delegate :currency, to: :account, allow_nil: true
  delegate :max_available_amount, to: :account, allow_nil: true, prefix: true

  after_create :start_process

  aasm column: 'state' do # rubocop:disable Metrics/BlockLength
    state :pending, initial: true
    state :processing
    state :completed
    state :failed

    event :process, after_commit: %i[mark_started transfer_amount] do
      transitions from: :pending, to: :processing
    end

    event :done, after_commit: %i[mark_completed] do
      transitions from: :processing, to: :completed
    end

    event :fail do
      transitions from: :processing, to: :failed
    end
  end

  def max_available_amount
    account_max_available_amount || 0.to_d
  end

  def mark_started
    update_column(:transaction_started_at, Time.zone.now)
  end

  def mark_completed
    update_column(:transaction_completed_at, Time.zone.now)
  end

  def to_account
    Account.find_by(currency: currency, internal: nil, number: number)
  end

  def fee
    FeeCalculatorService.new(amount: amount).fee
  end

  def start_process
    process!
  end

  private

  def transfer_amount
    self.transaction do
      if account.internal?
        transactions.create!(account: account, amount: amount, credit: true, transaction_type: :outgoing)
        transactions.create!(account: to_account, amount: amount, debit: true, transaction_type: :incoming)
      else
        transactions.create!(account: account, amount: fee, credit: true, transaction_type: :transfer_fee)
        transactions.create!(account: account, amount: amount, credit: true, transaction_type: :outgoing)
        transactions.create!(account: account.parent_account, amount: fee, debit: true, transaction_type: :fee_revenue)
        transactions.create!(account: to_account, amount: amount, debit: true, transaction_type: :incoming)
      end
      done!
      return
    end
  rescue => e
    Rails.logger.error(e)
    delay.fail!
    raise ActiveRecord::Rollback
  end
end
