class Account < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :parent_account, optional: true, class_name: 'Account'
  has_many :transaction_orders
  has_many :transactions

  delegate :email, to: :user, allow_nil: true, prefix: true
  after_create :generate_number, :assign_parent

  class << self
    def parent_account_for(currency)
      find_by(currency: currency, internal: true)
    end
  end

  def all_others
    Account.where.not(id: id).where(currency: currency, internal: nil)
  end

  def deposit(amount)
    TransferService.new(amount: amount, number: number).transfer_from(parent_account)
  end

  def credit(amount)
    raise 'Invalid amount' unless amount.positive?

    transaction do
      decrement!(:balance, amount)
      raise 'Balance went negative' if balance.negative?
    end
  end

  def debit(amount)
    raise 'Invalid amount' unless amount.positive?

    transaction do
      increment!(:balance, amount)
    end
  end

  def max_available_amount
    FeeCalculatorService.new(amount: balance).max_for_balance
  end

  private

  def generate_number
    number = (100_000_000 + id).to_s
    check_digit = Helpers::CheckDigitHelper.checksum731(number)
    update(number: "#{number}#{check_digit}")
  end

  def assign_parent
    return if internal?

    update(parent_account: self.class.parent_account_for('EUR'))
  end
end
