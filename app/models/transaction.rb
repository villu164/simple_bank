class Transaction < ApplicationRecord
  include AASM

  belongs_to :account
  belongs_to :transaction_order

  validates :transaction_type, presence: true

  scope :date_between, ->(search) {
    return none if search.invalid?

    where(created_at: search.start_date.beginning_of_day..search.end_date.end_of_day)
  }

  before_create :process_credit, :process_debit, :store_balance

  def store_balance
    self.balance = account.balance
  end

  def process_credit
    return unless credit?

    account.credit(amount)
  end

  def process_debit
    return unless debit?

    account.debit(amount)
  end

  def signed_amount
    return amount if debit?

    -amount
  end

  def html_class
    return 'text-green-400' if debit?
    
    'text-red-300'
  end
end
