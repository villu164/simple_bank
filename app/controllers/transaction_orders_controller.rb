class TransactionOrdersController < ApplicationController
  def index
    @search = TransactionSearchService.new(**search_params.deep_symbolize_keys)
    @simple = search_params.blank?
    return if current_user.account.blank?
    return unless @search.valid?

    @transactions = current_user.account.transactions.date_between(@search)
    @transaction_orders = current_user.account.transaction_orders
  end

  def new
    @transaction = TransferService.new
  end

  def create
    @transaction = TransferService.new(**transaction_params)

    if @transaction.transfer_from(current_user.account)
      flash.notice = 'Transfer successful'
      redirect_to transaction_orders_path
    else
      render :new
    end
  end

  private

  def transaction_params
    params.require(:transfer_service).permit(:amount, :number).to_h.deep_symbolize_keys
  end

  def search_params
    return {} if params[:transaction_search_service].blank?

    params.require(:transaction_search_service).permit(:start_date, :end_date).to_h
  end
end
