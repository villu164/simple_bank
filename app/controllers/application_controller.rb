class ApplicationController < ActionController::Base
  before_action :show_bank_offline!, unless: :bank_online?
  before_action :authenticate_user!, if: :bank_online?

  private

  def bank_online?
    Account.parent_account_for('EUR').present?
  end

  def show_bank_offline!
    render 'pages/offline', layout: 'offline'
  end
end
