source 'https://rubygems.org'
git_source(:github) { |repo| 'https://github.com/#{repo}.git' }

ruby '2.6.3'

gem 'rails', '~> 6.0.3', '>= 6.0.3.2'

gem 'aasm', '~> 5.1'
gem 'after_commit_everywhere', '~> 0.1.5'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'delayed_job_active_record', '~> 4.1'
gem 'devise', '~> 4.7'
gem 'faker', require: false
gem 'jbuilder', '~> 2.7'
gem 'pg', '>= 0.18', '< 2.0'
gem 'pry-rails', '~> 0.3.9', :groups => [:development, :test]
gem 'puma', '~> 4.1'
gem 'sass-rails', '>= 6'
gem 'simple_form', '~> 5.0'
gem 'slim', '~> 4.1'
gem 'turbolinks', '~> 5'
gem 'validates_timeliness', '~> 4.1'
gem 'webpacker', '~> 4.0'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'factory_bot_rails'
end

group :development do
  gem 'listen', '~> 3.2'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'spring'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'rspec-rails', '~> 4.0.1'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'simplecov', require: false
  gem 'timecop'
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
