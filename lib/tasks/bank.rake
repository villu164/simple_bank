# frozen_string_literal: true

namespace :bank do
  desc 'Setup EUR account if not exists and debit 1_000_000_000€'
  task setup: %i[create invest]

  desc 'Create EUR Bank account if not exists'
  task create: :environment do
    Account.where(currency: 'EUR', internal: true).first_or_create!(balance: 0)
  end

  desc 'Increase EUR Bank account balance by 1_000_000_000€'
  task invest: :environment do
    Account.find_by!(currency: 'EUR', internal: true).debit(1_000_000_000)
  end
end
