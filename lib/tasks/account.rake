# frozen_string_literal: true

namespace :account do
  desc 'Create a dummy accounts'
  task dummies: :environment do
    require 'faker'
    10.times do
      User.create!(
        email: Faker::Internet.email,
        password: 'foobar',
        password_confirmation: 'foobar'
      )
    end
  end
end
