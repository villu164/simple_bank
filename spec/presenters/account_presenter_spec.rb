require 'rails_helper'

RSpec.describe AccountPresenter, type: :model do
  subject { described_class.new(account) }
  let(:account) { double(Account) }

  describe '#name' do
    before do
      allow(account).to receive(:number).and_return(123)
      allow(account).to receive(:currency).and_return('EUR')
    end

    context 'internal EUR account' do
      before do
        allow(account).to receive(:internal?).and_return(true)
        allow(account).to receive(:user_email).and_return(nil)
      end

      it do
        expect(subject.name).to eq('EUR 123 - BANK')
      end
    end

    context 'internal EUR account that has email' do
      before do
        allow(account).to receive(:internal?).and_return(true)
        allow(account).to receive(:user_email).and_return('bank@example.com')
      end

      it do
        expect(subject.name).to eq('EUR 123 - BANK')
      end
    end

    context 'normal account without email' do
      before do
        allow(account).to receive(:internal?).and_return(nil)
        allow(account).to receive(:user_email).and_return(nil)
      end
    
      it do
        expect(subject.name).to eq('EUR 123 - Mr. Nobody')
      end
    end

    context 'normal account with email' do
      before do
        allow(account).to receive(:internal?).and_return(nil)
        allow(account).to receive(:user_email).and_return('asdf@asdf.asdf')
      end
    
      it do
        expect(subject.name).to eq('EUR 123 - asdf@asdf.asdf')
      end
    end

    context 'normal account with no email' do
      before do
        allow(account).to receive(:internal?).and_return(nil)
        allow(account).to receive(:user_email).and_return(nil)
      end
    
      it do
        expect(subject.name).to eq('EUR 123 - Mr. Nobody')
      end
    end
  end
end
