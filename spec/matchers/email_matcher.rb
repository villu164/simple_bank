# frozen_string_literal: true

require 'shoulda-matchers'
include Shoulda::Matchers::ActiveModel # rubocop:disable Style/MixinUsage

def validate_email_of(attr)
  EmailMatcher.new(attr)
end

class EmailMatcher < ValidationMatcher
  def matches?(subject)
    super(subject)

    allows_valid_email && disallows_valid_email
  end

  private

  def allows_valid_email
    allows_value_of('email@domain.com', I18n.t(:invalid, scope: %i[errors messages]))
  end

  def disallows_valid_email
    disallows_value_of('Joe Smith <email@domain.com>', I18n.t(:invalid, scope: %i[errors messages]))
  end

  def simple_description
    "validate that :#{@attribute} is a valid email"
  end
end
