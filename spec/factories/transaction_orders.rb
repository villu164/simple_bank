# frozen_string_literal: true

FactoryBot.define do
  factory :transaction_order do
    amount { 1 }
    number { '123' }
    account
  end
end
