# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'asdf@asdf.asdf' }
    password { 'asdf@asdf.asdf' }
    password_confirmation { 'asdf@asdf.asdf' }
  end
end
