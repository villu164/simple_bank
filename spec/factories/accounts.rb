# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    currency { 'EUR' }
    balance { 10 }
  end
end
