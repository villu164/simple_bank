require 'rails_helper'

RSpec.describe Account, type: :model do
  subject { build(:account) }
  before do
    Account.where(currency: 'EUR', internal: true).first_or_create!(balance: 1_000_000_000)
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user).optional }
    it { is_expected.to belong_to(:parent_account).optional }
    it { is_expected.to have_many(:transaction_orders) }
  end

  describe '#all_others' do
    subject { create(:account, currency: 'EUR') }
    let!(:matching_account) { create(:account, currency: 'EUR') }
    let!(:other_currency_account) { create(:account, currency: 'JPN') }
    let!(:internal_account) { create(:account, internal: true) }

    before do
      Account.where(currency: 'JPN', internal: true).first_or_create!(balance: 1_000_000_000)
    end

    it 'matches only non-internal same currency accounts' do
      expect(subject.all_others).to eq([matching_account])
    end
  end

  describe '#generate_number' do
    subject { build(:account, currency: 'EUR') }
    before do
      allow(subject).to receive(:id).and_return(666)
    end
    it do
      subject.send(:generate_number)
      expect(subject.number).to eq('1000006663')
    end
  end

  describe '#debit' do
    subject { build(:account, currency: 'EUR', balance: 0) }

    it do
      expect{ subject.debit(-1) }.to raise_error(RuntimeError)
    end

    it do
      expect{ subject.debit(0) }.to raise_error(RuntimeError)
    end

    it do
      expect{ subject.debit(1) }.to change { subject.balance }.by(1)
    end

    it do
      expect{ subject.debit(100) }.to change { subject.balance }.by(100)
    end
  end

  describe '#credit' do
    subject { build(:account, currency: 'EUR', balance: 10) }

    it do
      expect{ subject.credit(-1) }.to raise_error(RuntimeError)
    end

    it do
      expect{ subject.credit(0) }.to raise_error(RuntimeError)
    end

    it do
      expect{ subject.credit(0.01) }.to change { subject.balance }.by(-0.01)
    end

    it do
      expect{ subject.credit(10) }.to change { subject.balance }.by(-10)
    end

    it do
      expect{ subject.credit(11) }.to raise_error(RuntimeError)
    end
  end

  describe '#deposit' do
    subject { create(:account, currency: 'EUR', balance: 0) }

    it do
      expect{ subject.deposit(-1) }.to change { subject.reload.balance }.by(0)
    end

    it do
      expect{ subject.deposit(0) }.to change { subject.reload.balance }.by(0)
    end

    it do
      expect{ subject.deposit(0.01) }.to change { subject.reload.balance }.by(0.01)
    end

    it do
      expect{ subject.deposit(10) }.to change { subject.reload.balance }.by(10)
    end

    it 'Does not allow deposit greater than parent account has money' do
      expect{ subject.deposit(subject.parent_account.reload.balance + 1) }.to change { subject.reload.balance }.by(0)
    end
  end
end
