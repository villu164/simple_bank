require 'rails_helper'

RSpec.describe TransactionOrder, type: :model do
  subject { create(:transaction_order, number: to_account.number) }
  let(:to_account) { create(:account) }
  before do
    Account.where(currency: 'EUR', internal: true).first_or_create!(balance: 1_000_000_000)
  end
  describe 'associations' do
    it { is_expected.to have_many(:transactions) }
    it { is_expected.to belong_to(:account) }
  end

  describe 'validations' do
    let(:some_amount) { 666.66 }
    before do
      allow(subject).to receive(:max_available_amount).and_return(some_amount)
    end
    it { is_expected.to validate_presence_of(:amount) }
    it { is_expected.to validate_presence_of(:number) }
    it { is_expected.to validate_presence_of(:state) }
    it { is_expected.to validate_numericality_of(:amount).is_greater_than_or_equal_to(0.01) }
    it { is_expected.to validate_numericality_of(:amount).is_less_than_or_equal_to(some_amount) }
  end

  describe 'process' do
    context 'taking money from an internal account takes no fees' do
      subject { build(:transaction_order, number: to_account.number, account: account, amount: 10) }      
      let(:account) { create(:account, internal: true, currency: 'EUR', balance: 100) }

      it do
        expect do
          subject.save!
        end.to change { subject.account.reload.balance }.by(-10)
                 .and change { subject.to_account.reload.balance }.by(10)
      end
    end

    context 'taking money from an normal account takes fees' do
      subject { build(:transaction_order, number: to_account.number, account: account, amount: 10) }      
      let(:account) { create(:account, currency: 'EUR', balance: 100) }

      it do
        expect do
          subject.save!
        end.to change { subject.account.reload.balance }.by(-15)
                 .and change { subject.account.parent_account.reload.balance }.by(5)
                 .and change { subject.to_account.reload.balance }.by(10)
      end
    end

    context 'taking too much money from an normal account fails' do
      subject { build(:transaction_order, number: to_account.number, account: account, amount: 1000) }      
      let(:account) { create(:account, currency: 'EUR', balance: 100) }

      it do
        expect { subject.save! }.to raise_error { ActiveRecord::RecordInvalid }
      end
    end

    context 'taking too much money from an internal account fails' do
      subject { build(:transaction_order, number: to_account.number, account: account, amount: 1000) }      
      let(:account) { create(:account, internal: true, currency: 'EUR', balance: 100) }

      it do
        expect { subject.save! }.to raise_error { ActiveRecord::RecordInvalid }
      end
    end
  end
end
