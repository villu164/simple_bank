require 'rails_helper'

RSpec.describe Transaction, type: :model do
  subject { transaction_order.transactions.new(account: account, amount: 1, credit: true, transaction_type: :outgoing) }
  let(:account) { create(:account) }
  let(:to_account) { create(:account) }
  let(:transaction_order) { create(:transaction_order, number: to_account.number) }
  before do
    Account.where(currency: 'EUR', internal: true).first_or_create!(balance: 1_000_000_000)
  end
  describe 'associations' do
    it { is_expected.to belong_to(:account) }
    it { is_expected.to belong_to(:transaction_order) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:transaction_type) }
  end

  describe '#signed_amount' do
    context do
      before do
        allow(subject).to receive(:amount).and_return(5)
        allow(subject).to receive(:debit?).and_return(true)
      end
      it do
        expect(subject.signed_amount).to eq(5)
      end
    end

    context do
      before do
        allow(subject).to receive(:amount).and_return(5)
        allow(subject).to receive(:debit?).and_return(false)
      end
      it do
        expect(subject.signed_amount).to eq(-5)
      end
    end
  end

  describe '#html_class' do
    context do
      before do
        allow(subject).to receive(:debit?).and_return(true)
      end
      it do
        expect(subject.html_class).to eq('text-green-400')
      end
    end

    context do
      before do
        allow(subject).to receive(:debit?).and_return(false)
      end
      it do
        expect(subject.html_class).to eq('text-red-300')
      end
    end
  end

  describe '.date_between' do
    context do
      let!(:match) { transaction_order.transactions.create(account: account, amount: 1, credit: true, transaction_type: :outgoing, created_at: '2020-09-09 13:14:15') }
      let!(:no_match) { transaction_order.transactions.create(account: account, amount: 1, credit: true, transaction_type: :outgoing, created_at: '2020-09-10 13:14:15') }

      it 'matches when date includes it' do
        expect(described_class.date_between(OpenStruct.new(start_date: Date.parse('2020-09-09'), end_date: Date.parse('2020-09-09')))).to eq([match])
      end

      it 'matches nothing otherwise' do
        expect(described_class.date_between(OpenStruct.new(start_date: Date.parse('2020-09-08'), end_date: Date.parse('2020-09-08')))).to eq([])
      end
    end
  end
end
