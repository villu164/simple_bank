require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    Account.where(currency: 'EUR', internal: true).first_or_create!(balance: 1_000_000_000)
  end
  describe 'associations' do
    it { is_expected.to have_one(:account) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_email_of(:email) }
    it { is_expected.to validate_length_of(:password).is_at_least(6) }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  end
end
