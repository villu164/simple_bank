# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TransactionSearchService do
  before { Timecop.freeze('2020-02-20') }
  after { Timecop.return }

  describe '#start_date' do
    context 'no params' do
      it do
        expect(subject.start_date).to eq(Date.parse('2020-02-20'))
      end
    end

    context 'blank string given' do
      subject { described_class.new(start_date: ' ')}

      it do
        expect(subject.start_date).to eq(Date.parse('2020-02-20'))
      end
    end

    context 'invalid string given' do
      subject { described_class.new(start_date: 'asdf')}

      it do
        expect(subject.start_date).to eq(Date.parse('2020-02-20'))
      end
    end

    context 'start date given' do
      subject { described_class.new(start_date: '2020-03-14')}

      it do
        expect(subject.start_date).to eq(Date.parse('2020-03-14'))
      end
    end

    context 'start date and end date given' do
      subject { described_class.new(start_date: '2020-03-10', end_date: '2020-03-14')}

      it do
        expect(subject.start_date).to eq(Date.parse('2020-03-10'))
      end
    end
  end

  describe '#end_date' do
    context 'no params' do
      it do
        expect(subject.end_date).to eq(Date.parse('2020-02-20'))
      end
    end

    context 'blank string given' do
      subject { described_class.new(end_date: ' ')}

      it do
        expect(subject.end_date).to eq(Date.parse('2020-02-20'))
      end
    end

    context 'invalid string given' do
      subject { described_class.new(end_date: 'asdf')}

      it do
        expect(subject.end_date).to eq(Date.parse('2020-02-20'))
      end
    end

    context 'end date given' do
      subject { described_class.new(end_date: '2020-03-14')}

      it do
        expect(subject.end_date).to eq(Date.parse('2020-03-14'))
      end
    end
  end
end
