# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Helpers::CheckDigitHelper do
  describe '#checksum731' do
    [
      {
        code: '12131295', # https://www.pangaliit.ee/arveldused/viitenumber/7-3-1meetod
        checksum: 2
      },
      {
        code: '62231293',
        checksum: 0
      },
      {
        code: '1000000000',
        checksum: 3
      },
    ].each do |example|
      it "7-3-1 #{example[:code]} --> #{example[:checksum]}" do
        expect(described_class.checksum731(example[:code])).to eq(example[:checksum])
      end
    end
  end
end
