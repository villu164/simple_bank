# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FeeCalculatorService do
  describe '#fee' do
    context 'no params' do
      it do
        expect(subject.fee).to eq(5)
      end
    end

    context '0 given' do
      subject { described_class.new(amount: 0) }

      it do
        expect(subject.fee).to eq(5)
      end
    end

    context '-1 given' do
      subject { described_class.new(amount: -1) }

      it do
        expect(subject.fee).to eq(5)
      end
    end

    context '1000 given' do
      subject { described_class.new(amount: 1000) }

      it do
        expect(subject.fee).to eq(25)
      end
    end
  end

  describe '#max_for_balance' do
    context 'no params' do
      it do
        expect(subject.max_for_balance).to eq(0)
      end
    end

    context '0 given' do
      subject { described_class.new(amount: 0) }

      it do
        expect(subject.max_for_balance).to eq(0)
      end
    end

    context '-1 given' do
      subject { described_class.new(amount: -1) }

      it do
        expect(subject.max_for_balance).to eq(0)
      end
    end

    context '1000 given' do
      subject { described_class.new(amount: 1000) }

      it do
        expect(subject.max_for_balance).to eq(975.6)
      end
    end

    context '200 given' do
      subject { described_class.new(amount: 200) }

      it do
        expect(subject.max_for_balance).to eq(195)
      end
    end
  end
end
