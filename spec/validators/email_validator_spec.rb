# frozen_string_literal: true

require 'rails_helper'
RSpec.describe EmailValidator do
  let(:klass) do
    Class.new do
      include ActiveModel::Model

      def self.name
        'ActiveModelValidationsFake'
      end

      attr_accessor :attr_name
      validates :attr_name, email: true
    end
  end

  describe '#valid?', type: :model do
    subject { klass.new }

    it do
      is_expected.not_to allow_values(
        'plainaddress', # Missing @ sign and domain
        '#@%^%#$@#$@#.com', # Garbage
        '@domain.com', # Missing username
        'Joe Smith <email@domain.com>', # Encoded html within email is invalid
        'email.domain.com', # Missing @
        'email@domain@domain.com', # Two @ sign
        '.email@domain.com', # Leading dot in address is not allowed
        'email.@domain.com', # Trailing dot in address is not allowed
        'email..email@domain.com', # Multiple dots
        'ã‚ã„ã†ãˆãŠ@domain.com', # Unicode char as address
        'email@domain.com (Joe Smith)', # Text followed email is not allowed
        'email@domain', # Missing top level domain (.com/.net/.org/etc)
        'email@-domain.com', # Leading dash in front of domain is invalid
        'email@111.222.333.44444', # Invalid IP format
        'email@domain..com' # Multiple dot in the domain portion is invalid
      ).for(:attr_name)
    end
    it do
      is_expected.to allow_values(
        'email@domain.com', # Valid email
        'email@what4.com', # Domain contains a number
        'firstname.lastname@domain.com', # Email contains dot in the address field
        'email@subdomain.domain.com', # Email contains dot with subdomain
        'email@subdomain.what4.com', # Email contains dot with subdomain that contains a number
        'firstname+lastname@domain.com', # Plus sign is considered valid character
        '1234567890@domain.com', # Digits in address are valid
        'email@domain-one.com', # Dash in domain name is valid
        '_______@domain.com', # Underscore in the address field is valid
        'email@domain.name', # .name is valid Top Level Domain name
        'email@domain.co.jp', # Dot in Top Level Domain name also considered valid (use co.jp as example here)
        'firstname-lastname@domain.com', # Dash in address field is valid
        'z@zeh.com.br' # From http://zehfernando.com/2006/things-ive-learned-with-a-one-letter-email/
      ).for(:attr_name)
    end

    context 'allows blank' do
      it do
        is_expected.to allow_values(
          '',
          nil
        ).for(:attr_name)
      end
    end
  end
end
