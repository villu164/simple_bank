class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.references :account, null: false, foreign_key: true
      t.references :transaction_order, null: false, foreign_key: true
      t.decimal :amount, precision: 12, scale: 2, null: false
      t.decimal :balance, precision: 12, scale: 2, null: false
      t.boolean :debit
      t.boolean :credit
      t.string :transaction_type

      t.timestamps
    end
  end
end
