class CreateTransactionOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_orders do |t|
      t.references :account, null: false, foreign_key: true
      t.string :number, null: false
      t.decimal :amount, precision: 12, scale: 2, null: false
      t.string :state, null: false
      t.timestamp :transaction_started_at
      t.timestamp :transaction_completed_at

      t.timestamps
    end
  end
end
