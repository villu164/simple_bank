class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.references :parent_account, foreign_key: { to_table: :accounts }
      t.references :user, foreign_key: true
      t.string :number
      t.string :currency, null: false, default: 'EUR'
      t.boolean :internal
      t.decimal :balance, precision: 12, scale: 2, null: false, default: '0.0'

      t.timestamps
    end
  end
end
