Rails.application.routes.draw do
  root to: "transaction_orders#index"
  get 'transactions/new'
  get 'pages/offline', as: :offline
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :transaction_orders
end


